import axiosClient from '../configs/axiosClient';
import { HttpClient } from '../configs/httpClient';
import { HttpMethod } from '../configs/types/httpMethod';
import { UrlRequestBuilder } from '../configs/urlRequestConfig';
import { GetNewsRequest, NewsListResponse } from './news.model';

export class NewsApi {
  private client: HttpClient;

  constructor(client: HttpClient) {
    this.client = client;
  }

  async getNews(params: GetNewsRequest): Promise<NewsListResponse> {
    const request = UrlRequestBuilder.defaultRequest({
      path: '/top-headlines',
      method: HttpMethod.GET,
      params: { ...params, category: 'general' },
    });
    return await this.client.request(request);
  }
}

const newsApi = new NewsApi(axiosClient);
export default newsApi;
