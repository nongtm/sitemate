import axiosClient from '../configs/axiosClient';
import { HttpClient } from '../configs/httpClient';
import { ArticleResponse, GetNewsRequest } from './news.model';
import { NewsApi } from './newsApi';

class NewsRepository {
  private newsApi: NewsApi;

  constructor(client: HttpClient) {
    this.newsApi = new NewsApi(client);
  }

  async getNews(params: GetNewsRequest): Promise<ArticleResponse[]> {
    const response = await this.newsApi.getNews(params);
    return response.articles;
  }
}

const newsRepository = new NewsRepository(axiosClient);
export default newsRepository;
