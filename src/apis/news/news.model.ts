export interface NewsListResponse {
  status: string;
  totalResults: number;
  articles: ArticleResponse[];
}

export interface ArticleResponse {
  source: ArticleSourceResponse;
  author: string;
  title: string;
  description: string;
  url: string;
  urlToImage: string;
  publishedAt: string;
  content: string;
}

export interface ArticleSourceResponse {
  id: string;
  name: string;
}

export interface GetNewsRequest {
  q: string;
}
