import axios, { AxiosError } from 'axios';

import { HttpClient } from './httpClient';
import { UrlRequest, buildAxiosRequestConfig } from './urlRequestConfig';

axios.interceptors.request.use(
  config => {
    console.log(config.method?.toUpperCase(), config.url);
    console.log('Headers', config.headers);
    if (config.data) console.log('Body   ', config.data);
    if (config.params) console.log('Params ', config.params);
    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

axios.interceptors.response.use(
  response => {
    if (response.data) console.log('Body   ', JSON.stringify(response.data));
    return response;
  },
  error =>
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    Promise.reject(error),
);

export class AxiosClient implements HttpClient {
  request<T, R>(request: UrlRequest<T>): Promise<R> {
    const axiosRequestConfig = buildAxiosRequestConfig(request);
    return axios
      .request<R>(axiosRequestConfig)
      .then(response => response.data)
      .catch((error: AxiosError) => {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          throw error.response.data;
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request);
          throw error.request;
        } else {
          // Something happened in setting up the request that triggered an Error
          // console.log('Error', error.message);
          throw error.message;
        }
      });
  }
}

const axiosClient = new AxiosClient();
export default axiosClient;
