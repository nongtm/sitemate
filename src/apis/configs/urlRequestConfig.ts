import { AxiosRequestConfig } from 'axios';

import { HttpMethod } from './types/httpMethod';

export type RequestHeaders = Record<string, string | number | boolean>;

// export class ApiConfig {
//   private static _instance: ApiConfig = new ApiConfig();
//   constructor() {
//     if (!ApiConfig._instance) {
//       ApiConfig._instance = this;
//     }
//     return ApiConfig._instance;
//   }

//   static getInstance() {
//     return this._instance;
//   }
// }

export interface UrlRequest<T = unknown, R = unknown> {
  url: string;
  method: HttpMethod;
  headers?: RequestHeaders;
  body?: T;
  params?: R;
  timeout: number;
}

interface RequestConfig<T, R> {
  path: string;
  method: HttpMethod;
  headers?: RequestHeaders;
  body?: T;
  params?: R;
}

export class UrlRequestBuilder {
  static defaultRequest<T, R>(config: RequestConfig<T, R>): UrlRequest<T> {
    const baseUrl = 'https://newsapi.org';
    const url = new URL('/v2' + config.path, baseUrl).toJSON();
    const headers: RequestHeaders = {
      'Content-type': 'application/json',
    };
    return {
      url,
      method: config.method,
      headers: { ...headers, ...config.headers },
      body: config.body,
      params: { ...config.params, apiKey: '183daca270264bad86fc5b72972fb82a' },
      timeout: 30000,
    };
  }
}

export function buildAxiosRequestConfig<T, R>(
  urlRequest: UrlRequest<T, R>,
): AxiosRequestConfig<T> {
  const { url, headers, body, params, method, timeout } = urlRequest;
  return {
    url,
    headers,
    data: body,
    params,
    method,
    timeout,
  };
}
