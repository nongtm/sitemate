import { debounce } from 'lodash';
import { useCallback, useEffect, useState } from 'react';
import { FlatList, TextInput, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ArticleResponse } from 'src/apis/news/news.model';
import newsRepository from 'src/apis/news/newsRepository';

import NewsListItem from './components/NewsListItem';

const Separator = () => (
  <View style={{ width: '100%', height: 1, backgroundColor: '#aaaaaa' }} />
);

const NewsListScreen = () => {
  const [articles, setArticles] = useState<ArticleResponse[]>([]);
  const [inputValue, setInputValue] = useState<string>('');
  const [query, setQuery] = useState<string>('');

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debounceSearch = useCallback(
    debounce((input: string) => {
      setQuery(input);
    }, 1000),
    [],
  );

  useEffect(() => {
    (async () => {
      const data = await newsRepository.getNews({ q: query });
      setArticles(data);
    })();
  }, [query]);

  return (
    <SafeAreaView style={{ flex: 1, paddingHorizontal: 16 }}>
      <TextInput
        value={inputValue}
        onChangeText={text => {
          setInputValue(text);
          debounceSearch(text);
        }}
        placeholder="Search for news"
        style={{
          height: 48,
          borderColor: '#aaaaaa',
          borderWidth: 1,
          padding: 16,
          borderRadius: 4,
        }}
      />
      <FlatList
        data={articles}
        renderItem={({ item }) => <NewsListItem item={item} />}
        ItemSeparatorComponent={<Separator />}
      />
    </SafeAreaView>
  );
};

export default NewsListScreen;
