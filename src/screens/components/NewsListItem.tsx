import { Image, Text, View } from 'react-native';
import { ArticleResponse } from 'src/apis/news/news.model';

interface Props {
  item: ArticleResponse;
}

const NewsListItem: React.FC<Props> = ({ item }) => {
  return (
    <View style={{ flexDirection: 'row', paddingVertical: 16, gap: 16 }}>
      <Image
        source={{ uri: item.urlToImage }}
        style={{ width: 100, height: 100 }}
      />
      <View style={{ flex: 1 }}>
        <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 8 }}>
          {item.title}
        </Text>
        <Text style={{ marginBottom: 8 }}>{item.description}</Text>
        <View style={{ flexDirection: 'row', gap: 16 }}>
          <Text>{item.author}</Text>
          <Text>{item.publishedAt}</Text>
        </View>
      </View>
    </View>
  );
};

export default NewsListItem;
