/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import NewsListScreen from './screens/NewsListScreen';

const App = () => {
  return (
    <>
      <SafeAreaProvider>
        <GestureHandlerRootView style={{ flex: 1 }}>
          <NewsListScreen />
        </GestureHandlerRootView>
      </SafeAreaProvider>
    </>
  );
};

export default App;
